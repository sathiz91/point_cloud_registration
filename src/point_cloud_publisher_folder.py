#!/usr/bin/env python

import rospy
from std_msgs.msg import String

registration_complete = False

def registration_callback(msg):
    global registration_complete
    registration_complete = True

def publish_point_cloud_folder_path():
    rospy.init_node('point_cloud_folder_publisher', anonymous=True)
    pub = rospy.Publisher('/point_cloud_paths', String, queue_size=10)
    rospy.Subscriber('/registration_status', String, registration_callback)
    point_cloud_folder_path = "/home/sathish/catkin_ws/src/point_cloud_registration/data/"
    rate = rospy.Rate(1)  # 1 Hz
    while not rospy.is_shutdown() and not registration_complete:
        pub.publish(point_cloud_folder_path)
        rate.sleep()

if __name__ == '__main__':
    try:
        publish_point_cloud_folder_path()
    except rospy.ROSInterruptException:
        pass