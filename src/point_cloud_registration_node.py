#!/usr/bin/env python

import rospy
from sensor_msgs.msg import PointCloud2
import sensor_msgs.point_cloud2 as pc2
from std_msgs.msg import String
import open3d as o3d
import numpy as np
import gtsam
from pathlib import Path

class PointCloudRegistrationNode:
    def __init__(self):
        rospy.init_node('point_cloud_registration_node', anonymous=True)
        self.point_clouds = []
        self.pose_graph, self.initial_estimate = self.create_pose_graph()
        self.voxel_size = 0.05
        self.loop_closure_distance = 1.0
        rospy.Subscriber("/point_cloud_paths", String, self.point_cloud_paths_callback)
        self.merged_point_cloud_pub = rospy.Publisher("/merged_point_cloud", PointCloud2, queue_size=10)
        self.registration_complete_pub = rospy.Publisher("/registration_complete", String, queue_size=10)

    def point_cloud_paths_callback(self, msg):
        folder_path = msg.data
        rospy.loginfo(f"Reading point clouds from: {folder_path}")
        point_clouds = self.read_point_clouds(folder_path)

        self.total_point_clouds = len(point_clouds)

        for i, pcd in enumerate(point_clouds, start=1):
            rospy.loginfo(f"Processing point cloud {i}/{len(point_clouds)}")
            self.point_cloud_callback(pcd)

    def point_cloud_callback(self, pcd):
        self.point_clouds.append(pcd)

        if len(self.point_clouds) > 1:
            source_down, source_fpfh = self.downsample_and_compute_fpfh(self.point_clouds[-2], self.voxel_size)
            target_down, target_fpfh = self.downsample_and_compute_fpfh(self.point_clouds[-1], self.voxel_size)
            result = self.registration_ransac_based_on_feature_matching(source_down, target_down, source_fpfh, target_fpfh, self.voxel_size*1.5)

            rotation_v = result.transformation[:3, :3]
            translation_v = result.transformation[:3, 3]
            pose = gtsam.Pose3(gtsam.Rot3(rotation_v), gtsam.Point3(translation_v))

            odometry_noise = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.2, 0.2, 0.2, 0.1, 0.1, 0.1]))
            self.pose_graph.add(gtsam.BetweenFactorPose3(len(self.point_clouds) - 2, len(self.point_clouds) - 1, pose, odometry_noise))
            self.initial_estimate.insert(len(self.point_clouds) - 1, gtsam.Pose3(result.transformation))

            if len(self.point_clouds) > 2:
                for j in range(len(self.point_clouds) - 1):
                    if np.linalg.norm(gtsam.Pose3(result.transformation).translation() - self.initial_estimate.atPose3(j).translation()) < self.loop_closure_distance:
                        loop_closure_noise = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.3, 0.3, 0.3, 0.1, 0.1, 0.1]))
                        self.pose_graph.add(gtsam.BetweenFactorPose3(len(self.point_clouds) - 1, j, gtsam.Pose3(result.transformation).inverse(), loop_closure_noise))

            optimized_estimate = self.optimize_pose_graph(self.pose_graph, self.initial_estimate)
            merged_pcd = self.merge_point_clouds(optimized_estimate, self.point_clouds)

            rospy.loginfo(f"Merged point cloud has {len(merged_pcd.points)} points")

            merged_pcd_msg = pc2.create_cloud_xyz32(rospy.Header(frame_id='map'), np.asarray(merged_pcd.points))
            self.merged_point_cloud_pub.publish(merged_pcd_msg)


            o3d.io.write_point_cloud("merged_point_cloud.pcd", merged_pcd)
        
        if len(self.point_clouds) == self.total_point_clouds:
            rospy.loginfo("Point cloud registration complete")
            self.registration_complete_pub.publish(String("registration_complete"))
            rospy.signal_shutdown("All point clouds Processed")


    def read_point_clouds(self, folder_path):
        point_clouds = []

        print("FOLDER ", folder_path)
        for file_path in sorted(Path(folder_path).glob("*.pcd")):
            pcd = o3d.io.read_point_cloud(str(file_path))
            point_clouds.append(pcd)
        return point_clouds

    def downsample_and_compute_fpfh(self, pcd, voxel_size):
        pcd_down = pcd.voxel_down_sample(voxel_size=voxel_size)
        pcd_down.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
        pcd_fpfh = o3d.pipelines.registration.compute_fpfh_feature(pcd_down, o3d.geometry.KDTreeSearchParamHybrid(radius=0.25, max_nn=100))
        return pcd_down, pcd_fpfh

    def registration_ransac_based_on_feature_matching(self, source_down, target_down, source_fpfh, target_fpfh, distance_threshold):
        result = o3d.pipelines.registration.registration_ransac_based_on_feature_matching(
            source_down, target_down, source_fpfh, target_fpfh, distance_threshold,
            o3d.pipelines.registration.TransformationEstimationPointToPoint(False), 4, [
                o3d.pipelines.registration.CorrespondenceCheckerBasedOnEdgeLength(0.9),
                o3d.pipelines.registration.CorrespondenceCheckerBasedOnDistance(distance_threshold)
            ], o3d.pipelines.registration.RANSACConvergenceCriteria(4000000, 500)
        )
        return result

    def create_pose_graph(self):
        pose_graph = gtsam.NonlinearFactorGraph()
        initial_estimate = gtsam.Values()
        prior_noise = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.3, 0.3, 0.3, 0.1, 0.1, 0.1]))
        pose_graph.add(gtsam.PriorFactorPose3(0, gtsam.Pose3(), prior_noise))
        initial_estimate.insert(0, gtsam.Pose3())
        return pose_graph, initial_estimate

    def optimize_pose_graph(self, pose_graph, initial_estimate):
        optimizer = gtsam.LevenbergMarquardtOptimizer(pose_graph, initial_estimate)
        optimized_estimate = optimizer.optimize()
        return optimized_estimate

    def merge_point_clouds(self, optimized_estimate, point_clouds):
        merged_pcd = o3d.geometry.PointCloud()
        for i in range(len(point_clouds)):
            transformation_matrix = np.eye(4)
            transformation_matrix[:3, :3] = optimized_estimate.atPose3(i).rotation().matrix()
            transformation_matrix[:3, 3] = optimized_estimate.atPose3(i).translation()
            pcd = point_clouds[i].transform(transformation_matrix)
            merged_pcd += pcd
        return merged_pcd

    def run(self):
        rospy.spin()

if __name__ == "__main__":
    try:
        node = PointCloudRegistrationNode()
        node.run()
    except rospy.ROSInterruptException:
        pass