# Point Cloud Registration and Loop Closure in ROS
This project implements point cloud registration and loop closure in ROS using the GTSAM library and Open3D. It processes point cloud data stored in a folder, performs registration between consecutive point clouds, and detects loop closures to refine the registration. The merged point cloud can be visualized in RViz.

## Installation
1. Clone this repository into your catkin workspace:

```
cd ~/catkin_ws/src
git clone https://github.com/yourusername/point_cloud_registration.git
```

1. Install dependencies using the provided Anaconda environment:

```
conda env create -f environment.yml
```
1. Build your catkin workspace:

```
cd ~/catkin_ws
catkin_make
source ~/catkin_ws/devel/setup.bash
```
## Running the Code

1. Launch the ROS nodes:

```
roslaunch point_cloud_registration point_cloud_registration.launch
```
This command will start both the point_cloud_publisher_folder node and the point_cloud_registration_node node.

## Viewing the Merged Point Cloud
You can visualize the merged point cloud in RViz by subscribing to the /merged_point_cloud topic, which publishes PointCloud2 messages.

## File Descriptions
- point_cloud_registration_node.py: Node that processes the point clouds, performs registration, and detects loop closures.
- point_cloud_publisher_folder.py: Node that publishes the folder path containing the point cloud files.
- point_cloud_registration.launch: Launch file to start both nodes simultaneously.

## ROS Package Structure
<pre>
point_cloud_registration/
├── CMakeLists.txt
├── package.xml
├── launch
│   └── point_cloud_registration.launch
├── scripts
│   ├── point_cloud_registration_node.py
│   └── point_cloud_publisher_folder.py
├── data  <-- Sample data directory (to be replaced with your actual data)
└── environment.yml
</pre>

Replace data with your actual point cloud folder path containing the .pcd files.
# CODE DETAILS
## Registration between Consecutive Point Clouds:

1. The script registers each incoming point cloud with the previous one using RANSAC-based feature matching, producing a transformation between the two point clouds.

## Pose Graph Optimization:

1. The script maintains a pose graph where each node corresponds to a point cloud, and edges represent the transformations between consecutive point clouds.
1. After registering a new point cloud, the script adds an odometry factor (between the new and previous point cloud) to the pose graph.
If a potential loop closure is detected (based on a threshold distance between the current point cloud and previous point clouds), a loop closure factor is added to the pose graph. The loop closure factor compensates for accumulated drift in the odometry estimates.

## Optimization:

1. The pose graph is optimized using the GTSAM library, which refines the poses of all point clouds in the graph, taking into account both the odometry and loop closure factors.
1. This optimization step improves the accuracy of the point cloud poses, especially in cases where drift has occurred over time.

## Merging Point Clouds:

1. After optimizing the pose graph, the script merges all point clouds into a single point cloud using the optimized poses.
The merged point cloud represents the final map, where loop closures have been detected and corrected.
## Publishing Results:

The merged point cloud is published as a PointCloud2 message on the /merged_point_cloud topic for visualization in RViz.
